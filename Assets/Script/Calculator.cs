﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using TMPro;
using UnityEngine;

public class Calculator : MonoBehaviour {
	[SerializeField] TextMeshProUGUI ageText;
	[SerializeField] TextMeshProUGUI premiumText;
	[SerializeField] TMP_InputField coverAmountText;
	[SerializeField] TMP_Dropdown year;
	[SerializeField] TMP_Dropdown month;
	[SerializeField] TMP_Dropdown date;
	[SerializeField] TMP_Dropdown occupation;
	[SerializeField] Occupation occ;

	void Start() {
		coverAmountText.onValidateInput += delegate (string input, int charIndex, char addedChar) { return MyValidate(addedChar); };
	}

	public void calculate() {
		int age = int.Parse(ageText.text);
		float coverAmount = float.Parse(coverAmountText.text);
		float rating = 0f;
		string selectedOcc = occ.occupations[occupation.value];
		List<string> heavyOcc = Enum.GetNames(typeof(Occupation.HeavyManual)).ToList();
		List<string> lightOcc = Enum.GetNames(typeof(Occupation.LightManual)).ToList();
		List<string> professionalOcc = Enum.GetNames(typeof(Occupation.Professional)).ToList();
		List<string> whiteCollarOcc = Enum.GetNames(typeof(Occupation.WhiteCollar)).ToList();
		rating = heavyOcc.Contains(selectedOcc.ToString()) ? 1.75f :
		lightOcc.Contains(selectedOcc.ToString()) ? 1.5f :
		professionalOcc.Contains(selectedOcc.ToString()) ? 1.0f :
		whiteCollarOcc.Contains(selectedOcc.ToString()) ? 1.25f : 0.0f;
		if (rating != 0.0f && age > 0 && coverAmount > 0) {
			float premium = (coverAmount * rating * year.value) / 1000 * 12;
			premiumText.text = premium.ToString();
		} else {
			premiumText.text = "0.0";
		}
	}

	private char MyValidate(char addedChar) {
		if (!Char.IsNumber(addedChar)) {
			addedChar = '\0';
		}
		return addedChar;
	}
}
