﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using TMPro;

public class Occupation : MonoBehaviour {
	public enum LightManual {
		Cleaner,
		Florist
	}

	public enum Professional {
		Doctor
	}

	public enum WhiteCollar {
		Author
	}

	public enum HeavyManual {
		Farmer,
		Mechanic
	}

	public List<string> occupations = new List<string>();

	// Start is called before the first frame update
	void Start() {
		// Populate dropdown menu
		string[] lightManualOcc = Enum.GetNames(typeof(LightManual));
		string[] professionalOcc = Enum.GetNames(typeof(Professional));
		string[] whiteCollarOcc = Enum.GetNames(typeof(WhiteCollar));
		string[] heavyManualOcc = Enum.GetNames(typeof(HeavyManual));
		occupations.Add("Select Occupation");
		occupations.AddRange(lightManualOcc);
		occupations.AddRange(professionalOcc);
		occupations.AddRange(whiteCollarOcc);
		occupations.AddRange(heavyManualOcc);
		TMP_Dropdown occupationDropdown = gameObject.GetComponent<TMP_Dropdown>();
		occupationDropdown.ClearOptions();
		occupationDropdown.AddOptions(occupations);
	}

	// Update is called once per frame
	void Update() {

	}
}
