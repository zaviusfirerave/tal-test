﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.EventSystems;
using System;

public class Calendar : MonoBehaviour {
	[SerializeField] TMP_Dropdown year;
	[SerializeField] TMP_Dropdown month;
	[SerializeField] TMP_Dropdown date;
	[SerializeField] TMP_Text age;

	// Start is called before the first frame update
	void Start() {
		// Set up the year dropdown
		var dropdown = transform.GetComponent<TMP_Dropdown>();
		List<TMPro.TMP_Dropdown.OptionData> years = new List<TMPro.TMP_Dropdown.OptionData>();
		for (int i = 1; i <= 100; i++) {
			int yearOption = System.DateTime.Now.Year - i;
			years.Add(new TMP_Dropdown.OptionData(yearOption.ToString()));
		}
		year.AddOptions(years);
		year.onValueChanged.AddListener(delegate { yearSelected(year); });
	}

	private void yearSelected(TMP_Dropdown year) {
		age.text = year.value.ToString();
	}

	// Update is called once per frame
	void Update() {

	}
}
