Assumptions:
1. The Age field is showing the result of age calculation after the date of birth is set
2. The unit for age is years and can not be 0 or less
3. No client is older than 100 years old
4. Death - sum insured amount is an integer

Solution Clarifications:
Application is built using Unity game engine to speed up the UI design process. The code is written in C# which is the preferred programming language for Unity.
Please download the "Build" folder and open the "TAL Test" application.
The date dropdowns are not exactly dynamic since the premium calculation doesn't exactly need to know the extra day and months of the person's age based on my assumption.
What that means is that the date will just always display 31 different date selection instead of dynamically being updated depending on the month and year selected.
I have made this decision to save time on development and I am also aware that this doesn't give the accurate age input for the premium formula.